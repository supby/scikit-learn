__author__ = 'andrej'

from PIL import Image
import os
from time import time
from skimage.feature import hog
from sklearn import preprocessing
from sklearn.grid_search import GridSearchCV
from sklearn.externals import joblib
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

img_size = (64, 128)
pixels_per_cell = (6, 6)
cells_per_block = (3, 3)
orientations = 9
train_data_dir = 'E:\\myprojects\\bb\\data\\pd3\\train'

def local_hog(gim):
    return hog(gim, orientations=orientations, 
                    pixels_per_cell=pixels_per_cell,
                    cells_per_block=cells_per_block, 
                    visualise=False)

print 'Loading train data...'
t0 = time()

X = []
y_train = []
l = 0
ll = -1
for root, subFolders, files in os.walk(os.path.join(train_data_dir, 'pos')):
    # load pos
    for filename in files:
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        X.append(local_hog(gim))
        y_train.append(1)
        if ll != -1 and l == ll/2:
            break
        l += 1

l = 0
for root, subFolders, files in os.walk(os.path.join(train_data_dir, 'neg')):
    # load neg
    for filename in files:
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        X.append(local_hog(gim))
        y_train.append(0)
        if ll != -1 and l == ll/2:
            break
        l += 1

print("done in %0.3fs" % (time() - t0))

print("Scaling the training set")
t0 = time()

X_train = preprocessing.MinMaxScaler().fit_transform(X)

print("done in %0.3fs" % (time() - t0))

print("Fitting the classifier to the training set")
t0 = time()

clf = AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),
                         algorithm="SAMME.R",
                         n_estimators=300)
clf.fit(X_train, y_train)

print("done in %0.3fs" % (time() - t0))

# store clasifier
data = {'clf': clf}
joblib.dump(data, '../../res/pd64x128_2_hog_ppc6x6_ppb3x3_o9_aboost_sammer_minmax.pkl', compress=9)
