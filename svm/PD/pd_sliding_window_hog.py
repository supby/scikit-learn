from PIL import Image
from PIL import ImageDraw
from time import time
from sklearn.externals import joblib
from skimage.feature import hog
from sklearn import preprocessing
import os

pixels_per_cell = (6, 6)
cells_per_block = (3, 3)
orientations = 9

def local_hog(gim):
    return hog(gim, orientations=orientations,
                    pixels_per_cell=pixels_per_cell,
                    cells_per_block=cells_per_block,
                    visualise=False)

clf_img_size = (64, 128)
slide_step = 10
data = joblib.load('E:\dropbox\Dropbox\clf_res\pd64x128_hog_ppc6x6_ppb3x3_o9_aboost_sammer_minmax.pkl')
clf = data['clf']

#sl_wnd_sizes = [(30, 60),(48, 96),(64, 128),(80, 160),(120, 240),(240, 480)]
sl_wnd_sizes = [(275, 550)]
#sl_wnd_sizes = [(64, 128)]

img_name = 'E:\\myprojects\\bb\\data\\INRIAPerson\\Test\\pos\\crop_000021.png'
sl_im = Image.open(img_name).convert("L")

pd_boxes = []
ch_boxes = []
X = []
print("Scaning image...")
t0 = time()
for k in range(len(sl_wnd_sizes)):
    sl_wnd_size = sl_wnd_sizes[k]
    print '----------- sliding wnd:', sl_wnd_size
    sliding_box = (0, 0, sl_wnd_size[0], sl_wnd_size[1])
    for i in range(int(sl_im.size[1] / slide_step)):
        for j in range(int(sl_im.size[0] / slide_step)):
            img_d = sl_im.crop(sliding_box).convert("L").resize(clf_img_size, Image.ANTIALIAS)
            X.append(local_hog(img_d))
            ch_boxes.append(sliding_box)
            sliding_box = (sliding_box[0] + slide_step, sliding_box[1], sliding_box[0] + slide_step + sl_wnd_size[0],
                           sliding_box[1] + sl_wnd_size[1])
        sliding_box = (0, sliding_box[1] + slide_step, sl_wnd_size[0], sliding_box[1] + slide_step + sl_wnd_size[1])
print("done in %0.3fs" % (time() - t0))

print("Detecting PD...")
t0 = time()

X_test = preprocessing.MinMaxScaler().fit_transform(X)

res = clf.predict(X_test)
#res = clf.predict_proba(X_test)

for i in range(len(res)):    
#    if res[i][0] > 0.538:
    if res[i] == 1:
        pd_boxes.append(ch_boxes[i])
print 'Found=', len(pd_boxes)

#sl_im = sl_im.rotate(180)
for box in pd_boxes:
    draw = ImageDraw.Draw(sl_im)
    draw.rectangle([(box[0], box[1]), (box[2], box[3])], outline=256)
# sl_im.rotate(180).show()
#sl_im.rotate(180).save('F:\\tmp\\slwimgtmp\\'+img_name)
sl_im.save('F:\\tmp\\slwimgtmp\\'+os.path.basename(img_name))
# sl_im.show()




