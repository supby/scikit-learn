from PIL import Image
import os
import sys
from sklearn import preprocessing
from time import time
from sklearn.externals import joblib

img_size = (64, 128)

print 'Loading test data...'
t0 = time()

test_data_dir = '/home/andrej/data/pd/test2'
if sys.platform == 'win32':
    test_data_dir = 'E:\\myprojects\\bb\\data\\pedestrianFixed64x128\\test'
X = []
y_test = []
l = 0
ll = -1
for root, subFolders, files in os.walk(os.path.join(test_data_dir, 'pos')):
    # load pos
    for filename in files:
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        pixels = [float(v) for v in gim.getdata()]
        X.append(pixels)
        y_test.append(1)
        if ll != -1 and l == ll/2:
            break
        l += 1
l = 0
for root, subFolders, files in os.walk(os.path.join(test_data_dir, 'neg')):
    # load neg
    for filename in files:
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        pixels = [float(v) for v in gim.getdata()]
        X.append(pixels)
        y_test.append(0)
        if ll != -1 and l == ll/2:
            break
        l += 1

print("done in %0.3fs" % (time() - t0))

# X_test = X
X_test = preprocessing.MinMaxScaler(feature_range=(-1,1)).fit_transform(X)
# X_test = preprocessing.scale(X)

data = joblib\
        .load('/home/andrej/data/pd/res/pd64x128_pca4k_svm_model_wo_scale.pkl')
clf = data['clf']
pca = data['pca']

print("Projecting the input data on the eigenfaces orthonormal basis")
t0 = time()
X_test_pca = pca.transform(X_test)

print("done in %0.3fs" % (time() - t0))
# print X_test_pca
res = clf.predict(X_test_pca)

failures = 0
total_count = 0
for i in range(len(X_test_pca)):
    total_count += 1
    if res[i] != y_test[i]:
        failures += 1

print 'failures=', failures, 'total=', len(X_test_pca)
print 'eror percent = ', (float(failures)/len(X_test_pca))*100,'%'