__author__ = 'andrej'

from PIL import Image
import os
from sklearn import preprocessing
from skimage.feature import hog
from time import time
from sklearn.externals import joblib

img_size = (64, 128)
pixels_per_cell = (6, 6)
cells_per_block = (3, 3)
orientations = 9
train_data_dir = 'E:\\myprojects\\bb\\data\\pd2\\train'

def local_hog(gim):
    return hog(gim, orientations=orientations, 
                    pixels_per_cell=pixels_per_cell,
                    cells_per_block=cells_per_block, 
                    visualise=False)
                    
print 'Loading test data...'
t0 = time()

test_data_dir = 'E:\\myprojects\\bb\\data\\pd2\\test'

X = []
y_test = []
l = 0
ll = -1
for root, subFolders, files in os.walk(os.path.join(test_data_dir, 'pos')):
    # load pos
    for filename in files:
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        X.append(local_hog(gim))
        y_test.append(1)
        if ll != -1 and l == ll/2:
            break
        l += 1
l = 0
for root, subFolders, files in os.walk(os.path.join(test_data_dir, 'neg')):
    # load neg
    for filename in files:
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        X.append(local_hog(gim))
        y_test.append(0)
        if ll != -1 and l == ll/2:
            break
        l += 1

print("done in %0.3fs" % (time() - t0))

X_test = preprocessing.MinMaxScaler().fit_transform(X)

data = joblib\
        .load('../../res/pd64x128_hog_ppc6x6_ppb3x3_o9_svm_rbf_c1_prob_minmax.pkl')
clf = data['clf']

res = clf.predict(X_test)

failures = 0
total_count = 0
for i in range(len(X_test)):
    total_count += 1
    if res[i] != y_test[i]:
        failures += 1

print 'failures=', failures, 'total=', len(X_test)
print 'eror percent = ', (float(failures)/len(X_test))*100,'%'
