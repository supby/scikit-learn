from PIL import Image
import os
import sys
from sklearn.svm import SVC
from sklearn import preprocessing
from sklearn.decomposition import RandomizedPCA
from time import time
from sklearn.grid_search import GridSearchCV
from sklearn.externals import joblib


img_size = (48, 96)

n_components = 200
train_data_dir = '/home/andrej/data/pd/pd2/train'
if sys.platform == 'win32':
    train_data_dir = 'E:\\myprojects\\bb\\data\\pedestrianFixed64x128\\train'

print 'Loading train data...'
t0 = time()

X = []
y_train = []
l = 0
ll = -1
for root, subFolders, files in os.walk(os.path.join(train_data_dir, 'pos')):
    # load pos    
    for filename in files:        
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        pixels = [float(v) for v in gim.getdata()]
        X.append(pixels)
        y_train.append(1)
        if ll != -1 and l == ll/2:
            break
        l += 1
        
l = 0
for root, subFolders, files in os.walk(os.path.join(train_data_dir, 'neg')):
    # load neg
    for filename in files:
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        pixels = [float(v) for v in gim.getdata()]
        X.append(pixels)
        y_train.append(0)
        if ll != -1 and l == ll/2:
            break
        l += 1

print("done in %0.3fs" % (time() - t0))

# X_train = preprocessing.scale(X)
X_train = preprocessing.MinMaxScaler().fit_transform(X)

print("Extracting the top %d eigenfaces" % (n_components,))
t0 = time()
pca = RandomizedPCA(n_components=n_components, whiten=True).fit(X_train)
print("done in %0.3fs" % (time() - t0))

print("Projecting the input data on the eigenfaces orthonormal basis")
t0 = time()
X_train_pca = pca.transform(X_train)
print("done in %0.3fs" % (time() - t0))

print("Fitting the classifier to the training set")
t0 = time()
param_grid = {'C': [1e3, 5e3, 1e4, 5e4, 1e5],
              'gamma': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.1], }
clf = GridSearchCV(SVC(kernel='rbf', class_weight='auto'), param_grid)
clf = clf.fit(X_train_pca, y_train)
print("done in %0.3fs" % (time() - t0))
print("Best estimator found by grid search:")
print(clf.best_estimator_)

# store clasifier
data = {}
data['clf'] = clf
data['pca'] = pca
joblib.dump(data, '../../res/pd_pca200_svm_model_tmp1.pkl', compress=9)

    
    
    