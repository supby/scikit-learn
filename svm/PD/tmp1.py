import matplotlib.pyplot as plt

from skimage.feature import hog
from skimage import data, color, exposure

from PIL import Image


#image = color.rgb2gray(data.lena())
image = Image.open('/home/andrej/data/pd/sliding_window_test/zebra_crossing_630px.jpeg').convert("L").rotate(180)

fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
                    cells_per_block=(1, 1), visualise=True)

plt.figure(figsize=(8, 4))

plt.subplot(121).set_axis_off()
plt.imshow(image, cmap=plt.cm.gray)
plt.title('Input image')

# Rescale histogram for better display
# hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))
hog_image_rescaled = hog_image

plt.subplot(122).set_axis_off()
plt.imshow(hog_image_rescaled, cmap=plt.cm.gray)
plt.title('Histogram of Oriented Gradients')
plt.show()