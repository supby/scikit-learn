from PIL import Image
from PIL import ImageDraw
import os
import sys
from sklearn.svm import SVC
from sklearn import preprocessing
from sklearn.decomposition import RandomizedPCA
from time import time
from sklearn.grid_search import GridSearchCV
from sklearn.externals import joblib

clf_img_size = (64, 128)
slide_step = 5

sl_wnd_size = (64, 128)
sliding_box = (0, 0, sl_wnd_size[0], sl_wnd_size[1])
dir_to_extract = '/home/andrej/data/pd/test2'

sl_im = Image.open('/home/andrej/data/pd/sliding_window_test/frame0004.png').convert("L")

pd_boxes = []
print("Scaning image...")
t0 = time()
for k in range(5):
    print '----------- sliding box:', sliding_box
    for i in range(int(sl_im.size[1]/slide_step)):
        for j in range(int(sl_im.size[0]/slide_step)):
            img_d = sl_im.crop(sliding_box).convert("L").resize(clf_img_size, Image.ANTIALIAS)\
                         .save(os.path.join(dir_to_extract, '{0}_{1}_{2}.png'.format(k, i, j)))
            sliding_box = (sliding_box[0] + slide_step, sliding_box[1], sliding_box[0] + slide_step + sl_wnd_size[0], sliding_box[1] + sl_wnd_size[1])
        sliding_box = (0, sliding_box[1] + slide_step, sl_wnd_size[0], sliding_box[1] + slide_step + sl_wnd_size[1])
    sl_wnd_size = (sl_wnd_size[0] + 5, sl_wnd_size[1] + 10)
    sliding_box = (0, 0, sl_wnd_size[0], sl_wnd_size[1])
print("done in %0.3fs" % (time() - t0))






