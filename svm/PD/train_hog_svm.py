__author__ = 'andrej'

from PIL import Image
import os
from time import time
from skimage.feature import hog
from sklearn import preprocessing
from sklearn.grid_search import GridSearchCV
from sklearn.externals import joblib
from sklearn.svm import SVC
from sklearn.svm import LinearSVC

img_size = (64, 128)
pixels_per_cell = (6, 6)
cells_per_block = (3, 3)
orientations = 9
train_data_dir = 'E:\\myprojects\\bb\\data\\pd2\\train'

def local_hog(gim):
    return hog(gim, orientations=orientations, 
                    pixels_per_cell=pixels_per_cell,
                    cells_per_block=cells_per_block, 
                    visualise=False)

print 'Loading train data...'
t0 = time()

X = []
y_train = []
l = 0
ll = -1
for root, subFolders, files in os.walk(os.path.join(train_data_dir, 'pos')):
    # load pos
    for filename in files:
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        X.append(local_hog(gim))
        y_train.append(1)
        if ll != -1 and l == ll/2:
            break
        l += 1

l = 0
for root, subFolders, files in os.walk(os.path.join(train_data_dir, 'neg')):
    # load neg
    for filename in files:
        im = Image.open(os.path.join(root, filename))
        gim = im.convert("L").resize(img_size, Image.ANTIALIAS)
        X.append(local_hog(gim))
        y_train.append(0)
        if ll != -1 and l == ll/2:
            break
        l += 1

print("done in %0.3fs" % (time() - t0))

X_train = preprocessing.MinMaxScaler().fit_transform(X)

print("Fitting the classifier to the training set")
t0 = time()
#param_grid = {'C': [1e3, 5e3, 1e4, 5e4, 1e5],
#              'gamma': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.1], }
#clf = GridSearchCV(SVC(kernel='rbf', class_weight='auto'), param_grid)
#print("Best estimator found by grid search:")
#print(clf.best_estimator_)
C = 1.0
#clf = LinearSVC(C=C)
clf = SVC(kernel='rbf', C=C, probability=True)
clf = clf.fit(X_train, y_train)
print("done in %0.3fs" % (time() - t0))

# store clasifier
data = {'clf': clf}
joblib.dump(data, '../../res/pd64x128_hog_ppc6x6_ppb3x3_o9_svm_rbf_c1_prob_minmax.pkl', compress=9)
