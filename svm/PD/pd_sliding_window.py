from PIL import Image
from PIL import ImageDraw
from time import time
from sklearn.externals import joblib
from sklearn import preprocessing
import os

import pylab as pl
def plot_gallery(images, h, w, n_row=3, n_col=4):
    """Helper function to plot a gallery of portraits"""
    pl.figure(figsize=(1.8 * n_col, 2.4 * n_row))
    pl.subplots_adjust(bottom=0, left=.01, right=.99, top=.90, hspace=.35)
    for i in range(n_row * n_col):
        pl.subplot(n_row, n_col, i + 1)
        pl.imshow(images[i].reshape((h, w)), cmap=pl.cm.gray)
        pl.xticks(())
        pl.yticks(())

clf_img_size = (64, 128)
slide_step = 10

data = joblib.load('E:\dropbox\Dropbox\clf_res\pd64x128_pca2k_svm_model.pkl')
clf = data['clf']
pca = data['pca']

# eigenfaces = pca.components_.reshape((200, clf_img_size[1], clf_img_size[0]))
# plot_gallery(eigenfaces, clf_img_size[1], clf_img_size[0])
# pl.show()
# exit()

#sl_wnd_sizes = [(30, 60),(48, 96),(64, 128),(80, 160),(120, 240)]
sl_wnd_sizes = [(275, 550)]

img_name = 'E:\\myprojects\\bb\\data\\INRIAPerson\\Test\\pos\\crop_000021.png'
sl_im = Image.open(img_name).convert("L")

pd_boxes = []
ch_boxes = []
X = []
print("Scaning image...")
t0 = time()
for k in range(len(sl_wnd_sizes)):
    sl_wnd_size = sl_wnd_sizes[k]
    print '----------- sliding wnd:', sl_wnd_size
    sliding_box = (0, 0, sl_wnd_size[0], sl_wnd_size[1])
    for i in range(int(sl_im.size[1] / slide_step)):
        for j in range(int(sl_im.size[0] / slide_step)):
            img_d = sl_im.crop(sliding_box).convert("L").resize(clf_img_size, Image.ANTIALIAS)
            pixels = [float(v) for v in img_d.getdata()]
            X.append(pixels)
            ch_boxes.append(sliding_box)
            sliding_box = (sliding_box[0] + slide_step, sliding_box[1], sliding_box[0] + slide_step + sl_wnd_size[0],
                           sliding_box[1] + sl_wnd_size[1])
        sliding_box = (0, sliding_box[1] + slide_step, sl_wnd_size[0], sliding_box[1] + slide_step + sl_wnd_size[1])
print("done in %0.3fs" % (time() - t0))

print("Detecting PD...")
t0 = time()

X_test = preprocessing.MinMaxScaler().fit_transform(X)

X_test_pca = pca.transform(X_test)
res = clf.predict_proba(X_test_pca)
# res = clf.predict(X_test_pca)
print("done in %0.3fs" % (time() - t0))

for i in range(len(res)):
    if res[i][0] > 0.9999:
    # if res[i] == 1:
        pd_boxes.append(ch_boxes[i])
print 'Found=', len(pd_boxes)

sl_im = sl_im.rotate(180)
for box in pd_boxes:
    draw = ImageDraw.Draw(sl_im)
    draw.rectangle([(box[0], box[1]), (box[2], box[3])], outline=256)
sl_im.rotate(180).save('F:\\tmp\\slwimgtmp\\'+os.path.basename(img_name))





