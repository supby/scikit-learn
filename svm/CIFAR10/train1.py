from sklearn import svm
import csv

print 'Loading train data...'

trainData = []
with open('../data/train.csv', 'rb') as csvfile:
    r = csv.reader(csvfile)
    for row in r:
        trainData.append([float(cell) for cell in row])
        
print 'Train data has been loaded.'

print 'Loading train data labels...'

trainDataLabels = []
with open('../data/trainLabels.csv', 'rb') as csvfile:
    r = csv.reader(csvfile)
    for row in r:
        trainDataLabels.append(float(row[0]))
        
print 'Train data labels have been loaded.'

print 'Model fitting...'

clf = svm.SVC()
res = clf.fit(trainData, trainDataLabels)

print 'Model fitting has been fnished.'

print res


print 'Loading test data...'

testData = []
with open('../data/test.csv', 'rb') as csvfile:
    r = csv.reader(csvfile)
    for row in r:
        testData.append([float(cell) for cell in row])
        
print 'Test data has been loaded.'

res = clf.predict(testData)

print res


import csv
with open('../res/testLabels.csv', 'wb') as csvfile:
    w = csv.writer(csvfile)
    w.writerow(['Id','Solution'])
    i = 1
    for l in res:
        w.writerow([i, int(l)])
        i += 1
  

        
            
