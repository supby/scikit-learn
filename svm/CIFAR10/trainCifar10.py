# -*- coding: utf-8 -*-
"""
Created on Thu Dec 26 14:33:32 2013

@author: XUZ43075
"""

from PIL import Image
import os
import numpy
import csv
from sklearn import svm

labels = {'frog': 0,
          'truck': 1,
          'deer': 2,
          'automobile': 3,
          'bird': 4,
          'horse': 5,
          'ship': 6,
          'cat': 7,
          'airplane': 8,
          'dog': 9}

trainCount = 4000
featureCount = 32*32
# data_dir = '/home/andrej/data/cifar-10'
data_dir = 'E:\myprojects\\bb\data'
labelsCount = 10

print 'Loading train data...'

trainDataSet = []
for i in range(0, trainCount):
    im = Image.open(os.path.join(data_dir, 'train','{0}.png'.format(i+1))) 
    gim = im.convert("L")
    pixels = list(gim.getdata())
    trainDataSet.append(pixels)
    
outputs = []
i = 0
with open(os.path.join(data_dir, 'trainLabels.csv'), 'rb') as lbl_file:
    reader = csv.reader(lbl_file)
    for row in reader:
        if i > trainCount:
            break
        l = labels.get(row[1], None)
        if l:
            Y = numpy.zeros((labelsCount,), numpy.ubyte)
            Y[l] = 1
            outputs.append(Y)
            i += 1

print 'Train data has been loaded.'

print 'Model fitting...'

clf = svm.SVC()
res = clf.fit(trainDataSet, outputs)

print 'Model fitting has been fnished.'
